# Translation of Pamac.
# Copyright (C) 2013-2016 Manjaro Developers <manjaro-dev@manjaro.org>
# This file is distributed under the same license as the Pamac package.
# Guillaume Benoit <guillaume@manjaro.org>, 2013-2016.
#
# Translators:
# Al Manja <al.manja@gmx.com>, 2017
# ansich <mojmejlzaforume@gmail.com>, 2014
# Nenad Latinović <inactive+holden1987@transifex.com>, 2013
# ansich <mojmejlzaforume@gmail.com>, 2014-2015
# Nenad Latinović <inactive+holden1987@transifex.com>, 2013
# Nenad Latinović <inactive+holden1987@transifex.com>, 2013
# philm <philm@manjaro.org>, 2015
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pamac\n"
"Report-Msgid-Bugs-To: chris@cromer.cl\n"
"POT-Creation-Date: 2021-07-10 15:58-0400\n"
"PO-Revision-Date: 2017-07-30 02:40+0000\n"
"Last-Translator: philm <philm@manjaro.org>\n"
"Language-Team: Slovenian (http://www.transifex.com/manjarolinux/manjaro-"
"pamac/language/sl/)\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3);\n"

#: ../src/pamac-user-daemon/user_daemon.vala
msgid "Unknown"
msgstr "Neznano"

#: ../src/pamac-user-daemon/user_daemon.vala
#: ../src/pamac-manager/manager_window.vala
msgid "Explicitly installed"
msgstr "Izrecno nameščeno"

#: ../src/pamac-user-daemon/user_daemon.vala
#: ../src/pamac-manager/manager_window.vala
msgid "Installed as a dependency for another package"
msgstr "Nameščeno kot odvisnost za drugi paket"

#: ../src/pamac-user-daemon/user_daemon.vala
msgid "Yes"
msgstr "Da"

#: ../src/pamac-user-daemon/user_daemon.vala
msgid "No"
msgstr "Ne"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to initialize alpm library"
msgstr "Začenjanje alpm knjižnice je spodletelo"

#: ../src/pamac-system-daemon/system_daemon.vala ../src/transaction.vala
msgid "Authentication failed"
msgstr "Potrditev identitete ni uspela"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to synchronize any databases"
msgstr "Ni bilo mogoče sinhronizirati nobene podatkovne baze"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to init transaction"
msgstr "Ni bilo mogoče začeti transakcije"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to prepare transaction"
msgstr "Pripravljanje transakcije je spodletelo"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "target not found: %s"
msgstr "cilj ni bil najden: %s"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "package %s does not have a valid architecture"
msgstr "paket %s nima veljavne arhitekture"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "unable to satisfy dependency '%s' required by %s"
msgstr ""

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "installing %s (%s) breaks dependency '%s' required by %s"
msgstr ""

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "removing %s breaks dependency '%s' required by %s"
msgstr ""

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s and %s are in conflict"
msgstr "%s in %s sta v sporu"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s needs to be removed but it is a locked package"
msgstr "%s je potrebno odstraniti, vendar je ta paket zaklenjen"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to commit transaction"
msgstr "Ni bilo mogoče uveljaviti transakcije"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s exists in both %s and %s"
msgstr "%s obstoji tako v %s kot v %s"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s: %s already exists in filesystem"
msgstr "%s: %s že obstaja v datotečnem sistemu"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s is invalid or corrupted"
msgstr "%s je neveljaven ali pokvarjen"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s appears to be truncated: %jd/%jd bytes\n"
msgstr "%s izgleda prirezanih: %jd%jd bitov\n"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "failed retrieving file '%s' from %s : %s\n"
msgstr "napaka pri pridobivanju datoteke '%s' od %s : %s\n"

#: ../src/transaction.vala
msgid "Copy"
msgstr ""

#: ../src/transaction.vala
msgid "Refreshing mirrors list"
msgstr "Osveževanje seznama zrcal"

#: ../src/transaction.vala
msgid "Synchronizing package databases"
msgstr "Sinhroniziranje podatkovnih baz"

#: ../src/transaction.vala
msgid "Starting full system upgrade"
msgstr "Začenjanje nadgradnje celotnega sistema"

#: ../src/transaction.vala
msgid "Preparing"
msgstr "Pripravljam"

#: ../src/transaction.vala
#, c-format
msgid "Choose a provider for %s"
msgstr "Izberite ponudnika za %s"

#: ../src/transaction.vala
msgid "To remove"
msgstr "Za odstranitev"

#: ../src/transaction.vala
msgid "To downgrade"
msgstr "Za podgraditev"

#: ../src/transaction.vala
msgid "To build"
msgstr "Za izgradnjo"

#: ../src/transaction.vala
msgid "To install"
msgstr "Za namestitev"

#: ../src/transaction.vala
msgid "To reinstall"
msgstr "Za ponovno namestitev"

#: ../src/transaction.vala
msgid "To upgrade"
msgstr ""

#: ../src/transaction.vala ../src/pamac-manager/manager_window.vala
msgid "Total download size"
msgstr "Celotna velikost prenosa"

#: ../src/transaction.vala
#, c-format
msgid "Building %s"
msgstr ""

#: ../src/transaction.vala
msgid "Transaction cancelled"
msgstr "Transakcija preklicana"

#: ../src/transaction.vala
msgid "Checking dependencies"
msgstr "Preverjam odvisnosti"

#: ../src/transaction.vala
msgid "Checking file conflicts"
msgstr "Preverjam konflikte datotek"

#: ../src/transaction.vala
msgid "Resolving dependencies"
msgstr "Razrešujem odvisnosti"

#: ../src/transaction.vala
msgid "Checking inter-conflicts"
msgstr "Preverjanje medsebojnih sporov"

#: ../src/transaction.vala
#, c-format
msgid "Installing %s"
msgstr "Nameščanje %s"

#: ../src/transaction.vala
#, c-format
msgid "Upgrading %s"
msgstr "Nadgrajevanje %s"

#: ../src/transaction.vala
#, c-format
msgid "Reinstalling %s"
msgstr "Ponovno nameščanje %s"

#: ../src/transaction.vala
#, c-format
msgid "Downgrading %s"
msgstr "Podgrajevanje %s"

#: ../src/transaction.vala
#, c-format
msgid "Removing %s"
msgstr "Odstranjevanje %s"

#: ../src/transaction.vala
msgid "Checking integrity"
msgstr "Preverjanje celovitosti"

#: ../src/transaction.vala
msgid "Loading packages files"
msgstr "Nalaganje datotek paketov"

#: ../src/transaction.vala
#, c-format
msgid "Configuring %s"
msgstr "Konfiguriranje %s"

#: ../src/transaction.vala
msgid "Downloading"
msgstr "Nalaganje"

#: ../src/transaction.vala
#, c-format
msgid "Downloading %s"
msgstr "Nalaganje %s"

#: ../src/transaction.vala
msgid "Checking available disk space"
msgstr "Preverjanje razpoložljivega prostora"

#: ../src/transaction.vala
#, c-format
msgid "%s optionally requires %s"
msgstr "%s izbirno zahteva %s"

#: ../src/transaction.vala
#, c-format
msgid "Database file for %s does not exist"
msgstr "Datoteka podatkovne baze za %s ne obstaja"

#: ../src/transaction.vala
msgid "Checking keyring"
msgstr "Preverjanje obroča ključev"

#: ../src/transaction.vala
msgid "Downloading required keys"
msgstr "Nalaganje zahtevanih ključev"

#: ../src/transaction.vala
#, c-format
msgid "%s installed as %s.pacnew"
msgstr "%s nameščeno kot %s.pacnew"

#: ../src/transaction.vala
#, c-format
msgid "%s installed as %s.pacsave"
msgstr "%s nameščeno kot %s.pacsave"

#: ../src/transaction.vala
msgid "Running pre-transaction hooks"
msgstr ""

#: ../src/transaction.vala
msgid "Running post-transaction hooks"
msgstr ""

#: ../src/transaction.vala
#, c-format
msgid "About %u seconds remaining"
msgstr "Preostalo približno %u sekund"

#: ../src/transaction.vala
#, c-format
msgid "About %lu minute remaining"
msgid_plural "About %lu minutes remaining"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: ../src/transaction.vala
#, c-format
msgid "Refreshing %s"
msgstr "Osveževanje %s"

#: ../src/transaction.vala
msgid "Error"
msgstr "Napaka"

#: ../src/transaction.vala
msgid "Warning"
msgstr "Opozorilo"

#: ../src/transaction.vala ../data/interface/progress_dialog.ui
#: ../data/interface/history_dialog.ui
msgid "_Close"
msgstr "_Zapri"

#: ../src/transaction.vala
msgid "Nothing to do"
msgstr "Ni nobenih opravil"

#: ../src/transaction.vala
msgid "Transaction successful"
msgstr ""

#: ../src/transaction.vala
msgid "The transaction has been completed successfully"
msgstr ""

#: ../src/transaction.vala
msgid "Transaction successfully finished"
msgstr "Transakcija uspešno zaključena"

#: ../src/transaction.vala
msgid "Transaction failed"
msgstr ""

#: ../src/transaction.vala
msgid "The transaction failed and no packages have been updated/installed"
msgstr ""

#: ../src/pamac-install/installer.vala
msgid "Unable to lock database!"
msgstr ""

#: ../src/pamac-tray/tray.vala ../src/pamac-manager/manager_window.vala
msgid "Your system is up-to-date"
msgstr "Vaš sistem je posodobljen"

#: ../src/pamac-tray/tray.vala ../src/pamac-manager/manager_window.vala
msgid "Package Manager"
msgstr "Upravljalnik paketov"

#: ../src/pamac-tray/tray.vala
msgid "_Quit"
msgstr "_Izhod"

#: ../src/pamac-tray/tray.vala
#, c-format
msgid "%u available update"
msgid_plural "%u available updates"
msgstr[0] "Na voljo je %u posodobitev"
msgstr[1] "Na voljo sta %u posodobitvi"
msgstr[2] "Na voljo je %u posodobitev"
msgstr[3] "Na voljo je %u posodobitev"

#: ../src/pamac-tray/tray.vala ../src/pamac-manager/manager_window.vala
#: ../data/interface/progress_dialog.ui ../data/interface/manager_window.ui
msgid "Details"
msgstr "Podrobnosti"

#: ../src/pamac-manager/manager.vala
msgid "Pamac is already running"
msgstr "Pamac je že v teku"

#: ../src/pamac-manager/manager.vala
msgid "Refresh Databases"
msgstr ""

#: ../src/pamac-manager/manager.vala ../data/interface/manager_window.ui
msgid "View History"
msgstr "Glej zgodovino"

#: ../src/pamac-manager/manager.vala ../src/pamac-manager/manager_window.vala
#: ../data/interface/manager_window.ui
msgid "Install Local Packages"
msgstr "Namesti krajevne pakete"

#: ../src/pamac-manager/manager.vala ../data/interface/manager_window.ui
msgid "Preferences"
msgstr "Nastavitve"

#: ../src/pamac-manager/manager.vala ../data/interface/manager_window.ui
msgid "About"
msgstr "O programu Pamac"

#: ../src/pamac-manager/manager.vala
msgid "Quit"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Deselect"
msgstr "Prekliči izbiro"

#: ../src/pamac-manager/manager_window.vala
msgid "Upgrade"
msgstr ""

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Install"
msgstr "Namesti"

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Remove"
msgstr "Odstrani"

#: ../src/pamac-manager/manager_window.vala
msgid "Waiting for another package manager to quit"
msgstr "Čakanje na izhod drugega paketnega upravljalnika"

#: ../src/pamac-manager/manager_window.vala
#, c-format
msgid "%u pending operation"
msgid_plural "%u pending operations"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: ../src/pamac-manager/manager_window.vala
msgid "Installed"
msgstr "Nameščeno"

#: ../src/pamac-manager/manager_window.vala
msgid "Orphans"
msgstr "Osirotele datoteke"

#: ../src/pamac-manager/manager_window.vala
msgid "Foreign"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Pending"
msgstr "V čakanju"

#: ../src/pamac-manager/manager_window.vala
msgid "Install Reason"
msgstr "Namestitveni razlog"

#: ../src/pamac-manager/manager_window.vala
msgid "Mark as explicitly installed"
msgstr "Označi kot izrecno nameščeno"

#: ../src/pamac-manager/manager_window.vala
msgid "Licenses"
msgstr "Licence"

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Repository"
msgstr "Skladišče"

#: ../src/pamac-manager/manager_window.vala
msgid "Download size"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Installed size"
msgstr ""

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Groups"
msgstr "Skupine"

#: ../src/pamac-manager/manager_window.vala
msgid "Packager"
msgstr "Paketni program"

#: ../src/pamac-manager/manager_window.vala
msgid "Build Date"
msgstr "Datum razvoja"

#: ../src/pamac-manager/manager_window.vala
msgid "Install Date"
msgstr "Datum namestitve"

#: ../src/pamac-manager/manager_window.vala
msgid "Signatures"
msgstr "Podpisi"

#: ../src/pamac-manager/manager_window.vala
msgid "Backup files"
msgstr "Varnostne datoteke"

#: ../src/pamac-manager/manager_window.vala
msgid "Depends On"
msgstr "Odvisen od"

#: ../src/pamac-manager/manager_window.vala
msgid "Optional Dependencies"
msgstr "Izbirne odvisnosti"

#: ../src/pamac-manager/manager_window.vala
msgid "Required By"
msgstr "Zahtevano s strani"

#: ../src/pamac-manager/manager_window.vala
msgid "Optional For"
msgstr "Izbirno za"

#: ../src/pamac-manager/manager_window.vala
msgid "Provides"
msgstr "Zagotavlja"

#: ../src/pamac-manager/manager_window.vala
msgid "Replaces"
msgstr "Nadomesti"

#: ../src/pamac-manager/manager_window.vala
msgid "Conflicts With"
msgstr "Je v sporu s"

#: ../src/pamac-manager/manager_window.vala
msgid "Package Base"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Maintainer"
msgstr "Vzdrževalec"

#: ../src/pamac-manager/manager_window.vala
msgid "First Submitted"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Last Modified"
msgstr "Zadnja sprememba"

#: ../src/pamac-manager/manager_window.vala
msgid "Votes"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Out of Date"
msgstr "Zastarel"

#: ../src/pamac-manager/manager_window.vala
msgid "Make Dependencies"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Check Dependencies"
msgstr ""

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Name"
msgstr "Ime"

#: ../src/pamac-manager/manager_window.vala
#: ../data/interface/transaction_sum_dialog.ui
#: ../data/interface/manager_window.ui
#: ../data/interface/choose_ignorepkgs_dialog.ui
msgid "_Cancel"
msgstr "_Prekliči"

#: ../src/pamac-manager/manager_window.vala
msgid "_Open"
msgstr "_Odpri"

#: ../src/pamac-manager/manager_window.vala
msgid "Alpm Package"
msgstr "Paket Alpm"

#: ../src/pamac-manager/manager_window.vala
msgid "A Gtk3 frontend for libalpm"
msgstr "Gtk3 začelje za libalpm"

#: ../src/preferences_dialog.vala ../data/interface/preferences_dialog.ui
msgid "How often to check for updates, value in hours"
msgstr "Kako pogosto naj se preverja za posodobitve, vrednost v urah"

#: ../src/preferences_dialog.vala ../data/interface/preferences_dialog.ui
msgid "Number of versions of each package to keep in the cache"
msgstr "Število verzij za vsak paket shranjenih v predpomnilniku"

#: ../src/preferences_dialog.vala
msgid "Build directory"
msgstr ""

#: ../src/preferences_dialog.vala
msgid "Package directory"
msgstr ""

#: ../src/preferences_dialog.vala
msgid "Worldwide"
msgstr "Svetovno"

#: ../src/preferences_dialog.vala
msgid "Speed"
msgstr "Hitrost"

#: ../src/preferences_dialog.vala
msgid "Random"
msgstr "Naključno"

#: ../data/polkit/org.pamac.policy.in
msgid "Authentication is required"
msgstr "Zahtevana je overitev"

#: ../data/interface/choose_provider_dialog.ui
msgid "Choose a Provider"
msgstr "Izberi ponudnika"

#: ../data/interface/choose_provider_dialog.ui
#: ../data/interface/choose_ignorepkgs_dialog.ui
msgid "Choose"
msgstr "Izberite"

#: ../data/interface/progress_dialog.ui
msgid "Progress"
msgstr "Potek"

#: ../data/interface/history_dialog.ui
msgid "Pamac History"
msgstr "Zgodovina Pamac"

#: ../data/interface/transaction_sum_dialog.ui
msgid "Transaction Summary"
msgstr "Povzetek transakcije"

#: ../data/interface/transaction_sum_dialog.ui
msgid "Commit"
msgstr "Uveljavi"

#: ../data/interface/manager_window.ui
msgid "Refresh databases"
msgstr "Osveži baze podatkov"

#: ../data/interface/manager_window.ui
msgid "Search"
msgstr "Išči"

#: ../data/interface/manager_window.ui
msgid "State"
msgstr "Stanje"

#: ../data/interface/manager_window.ui
msgid "Repositories"
msgstr "Skladišča"

#: ../data/interface/manager_window.ui
msgid "Updates"
msgstr ""

#: ../data/interface/manager_window.ui
msgid "Version"
msgstr "Različica"

#: ../data/interface/manager_window.ui
msgid "Size"
msgstr "Velikost"

#: ../data/interface/manager_window.ui
msgid "Popularity"
msgstr ""

#: ../data/interface/manager_window.ui ../data/interface/preferences_dialog.ui
msgid "AUR"
msgstr "AUR"

#: ../data/interface/manager_window.ui
msgid "Browse"
msgstr ""

#: ../data/interface/manager_window.ui
msgid "Reinstall"
msgstr "Ponovno namesti"

#: ../data/interface/manager_window.ui
msgid "Dependencies"
msgstr "Odvisnosti"

#: ../data/interface/manager_window.ui
msgid "Files"
msgstr "Datoteke"

#: ../data/interface/manager_window.ui
msgid "_Apply"
msgstr "_Uveljavi"

#: ../data/interface/preferences_dialog.ui
msgid "Remove unrequired dependencies"
msgstr "Odstrani nezahtevane odvisnosti"

#: ../data/interface/preferences_dialog.ui
msgid ""
"When removing a package, also remove its dependencies that are not required "
"by other packages"
msgstr ""
"Ob odstranjevanju paketov odstrani tudi tiste odvisnosti, ki niso zahtevani "
"s strani drugih paketov"

#: ../data/interface/preferences_dialog.ui
msgid "Check available disk space"
msgstr "Preveri razpoložljiv prostor na disku"

#: ../data/interface/preferences_dialog.ui
msgid "Check for updates"
msgstr "Preveri za posodobitve"

#: ../data/interface/preferences_dialog.ui
msgid "Update files databases (more details but slower)"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Hide tray icon when no update available"
msgstr "Skrij ikono sistemske vrstice, če ni na voljo nobenih posodobitev"

#: ../data/interface/preferences_dialog.ui
msgid "Ignore upgrades for:"
msgstr "Spreglej nadgradnje za:"

#: ../data/interface/preferences_dialog.ui
msgid "General"
msgstr "Splošno"

#: ../data/interface/preferences_dialog.ui
msgid "Background color:"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Text color:"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Font:"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Terminal"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Use mirrors from:"
msgstr "Uporabi zrcalne strežnike iz:"

#: ../data/interface/preferences_dialog.ui
msgid "Sort mirrors by:"
msgstr "Razvrsti zrcalne strežnike po:"

#: ../data/interface/preferences_dialog.ui
msgid "Refresh Mirrors List"
msgstr "Osveži seznam zrcal"

#: ../data/interface/preferences_dialog.ui
msgid "Official Repositories"
msgstr "Uradna skladišča"

#: ../data/interface/preferences_dialog.ui
msgid ""
"AUR is a community maintained repository so it presents potential risks and "
"problems.\n"
"All AUR users should be familiar with the build process."
msgstr ""
"AUR je skladišče, ki ga urejajo uporabniki, zaradi česar je lahko "
"potencialni vir tveganj in težav.\n"
"Priporočljivo je, da so vsi uporabniki AUR seznanjeni s procesom izgradnje "
"programov."

#: ../data/interface/preferences_dialog.ui
msgid "Enable AUR support"
msgstr "Omogoči podporo AUR"

#: ../data/interface/preferences_dialog.ui
msgid "Allow Pamac to search and install packages from AUR"
msgstr "Dovoli Pamacu da išče in namešča pakete iz AUR"

#: ../data/interface/preferences_dialog.ui
msgid "Search in AUR by default"
msgstr "Privzeto iskanje v AUR"

#: ../data/interface/preferences_dialog.ui
msgid "Check for updates from AUR"
msgstr "Preveri za posodobitve iz AUR"

#: ../data/interface/preferences_dialog.ui
msgid "Keep built packages"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Remove only the versions of uninstalled packages"
msgstr "Odstrani le različice nenameščenih paketov"

#: ../data/interface/preferences_dialog.ui
msgid "Clean cache"
msgstr "Izbriši predpomnilnik"

#: ../data/interface/preferences_dialog.ui
msgid "Cache"
msgstr "Predpomnilnik"

#: ../data/interface/choose_ignorepkgs_dialog.ui
msgid "Choose Ignored Upgrades"
msgstr "Paketi, ki naj se ne posodabljajo"

#~ msgid "Checking delta integrity"
#~ msgstr "Preverjanje celovitosti delta"

#~ msgid "Applying deltas"
#~ msgstr "Apliciranje delt"

#, c-format
#~ msgid "Generating %s with %s"
#~ msgstr "Generiranje %s z %s"

#~ msgid "Generation succeeded"
#~ msgstr "Generiranje uspešno"

#~ msgid "Generation failed"
#~ msgstr "Generiranje ni bilo uspešno"
