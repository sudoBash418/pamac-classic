# Translation of Pamac.
# Copyright (C) 2013-2016 Manjaro Developers <manjaro-dev@manjaro.org>
# This file is distributed under the same license as the Pamac package.
# Guillaume Benoit <guillaume@manjaro.org>, 2013-2016.
#
# Translators:
# Hsiu-Ming Chang <cges30901@gmail.com>, 2016
# Hsiu-Ming Chang <cges30901@gmail.com>, 2016
# Jeff Huang <s8321414@gmail.com>, 2014-2017
# Jeff Huang <s8321414@gmail.com>, 2013-2014
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pamac\n"
"Report-Msgid-Bugs-To: chris@cromer.cl\n"
"POT-Creation-Date: 2021-07-10 15:58-0400\n"
"PO-Revision-Date: 2017-07-30 12:17+0000\n"
"Last-Translator: Jeff Huang <s8321414@gmail.com>\n"
"Language-Team: Chinese (Taiwan) (http://www.transifex.com/manjarolinux/"
"manjaro-pamac/language/zh_TW/)\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../src/pamac-user-daemon/user_daemon.vala
msgid "Unknown"
msgstr "未知的"

#: ../src/pamac-user-daemon/user_daemon.vala
#: ../src/pamac-manager/manager_window.vala
msgid "Explicitly installed"
msgstr "單獨指定安裝"

#: ../src/pamac-user-daemon/user_daemon.vala
#: ../src/pamac-manager/manager_window.vala
msgid "Installed as a dependency for another package"
msgstr "作爲其他套件的依賴關係安裝"

#: ../src/pamac-user-daemon/user_daemon.vala
msgid "Yes"
msgstr "是"

#: ../src/pamac-user-daemon/user_daemon.vala
msgid "No"
msgstr "否"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to initialize alpm library"
msgstr "初始化 alpm 函式庫失敗"

#: ../src/pamac-system-daemon/system_daemon.vala ../src/transaction.vala
msgid "Authentication failed"
msgstr "授權失敗"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to synchronize any databases"
msgstr "同步任何套件庫失敗"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to init transaction"
msgstr "初始化事務處理失敗"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to prepare transaction"
msgstr "準備事務處理失敗"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "target not found: %s"
msgstr "目標找不到： %s"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "package %s does not have a valid architecture"
msgstr "軟體包 %s 沒有有效的架構"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "unable to satisfy dependency '%s' required by %s"
msgstr "無法滿足依賴關係 '%s' 由 %s 所要求"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "installing %s (%s) breaks dependency '%s' required by %s"
msgstr "正在安裝%s (%s) 破壞了依賴關係 '%s' 由 %s 所要求"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "removing %s breaks dependency '%s' required by %s"
msgstr "正在移除 %s 破壞依賴關係 '%s' 由 %s 所要求"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s and %s are in conflict"
msgstr "%s 及 %s 衝突"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s needs to be removed but it is a locked package"
msgstr "%s 需要被移除，但它是一個被鎖定的軟體包"

#: ../src/pamac-system-daemon/system_daemon.vala
msgid "Failed to commit transaction"
msgstr "提交事務處理失敗"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s exists in both %s and %s"
msgstr "%s 同時存在於 %s 及 %s"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s: %s already exists in filesystem"
msgstr "%s ： %s 已在檔案系統中存在"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s is invalid or corrupted"
msgstr "%s 是無效或損毀的"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "%s appears to be truncated: %jd/%jd bytes\n"
msgstr "%s 似乎被截斷了：%jd/%jd 位元組\n"

#: ../src/pamac-system-daemon/system_daemon.vala
#, c-format
msgid "failed retrieving file '%s' from %s : %s\n"
msgstr "擷取檔案「%s」失敗，從 %s：%s\n"

#: ../src/transaction.vala
msgid "Copy"
msgstr "複製"

#: ../src/transaction.vala
msgid "Refreshing mirrors list"
msgstr "正在重新整理鏡像列表"

#: ../src/transaction.vala
msgid "Synchronizing package databases"
msgstr "正在同步套件庫"

#: ../src/transaction.vala
msgid "Starting full system upgrade"
msgstr "正在開始全系統升級"

#: ../src/transaction.vala
msgid "Preparing"
msgstr "準備中"

#: ../src/transaction.vala
#, c-format
msgid "Choose a provider for %s"
msgstr "選擇一個 %s 的提供者"

#: ../src/transaction.vala
msgid "To remove"
msgstr "移除"

#: ../src/transaction.vala
msgid "To downgrade"
msgstr "降級"

#: ../src/transaction.vala
msgid "To build"
msgstr "構建"

#: ../src/transaction.vala
msgid "To install"
msgstr "安裝"

#: ../src/transaction.vala
msgid "To reinstall"
msgstr "重新安裝"

#: ../src/transaction.vala
msgid "To upgrade"
msgstr ""

#: ../src/transaction.vala ../src/pamac-manager/manager_window.vala
msgid "Total download size"
msgstr "總計下載大小"

#: ../src/transaction.vala
#, c-format
msgid "Building %s"
msgstr "正在構建 %s"

#: ../src/transaction.vala
msgid "Transaction cancelled"
msgstr "已取消事務處理"

#: ../src/transaction.vala
msgid "Checking dependencies"
msgstr "正在檢查依賴關係"

#: ../src/transaction.vala
msgid "Checking file conflicts"
msgstr "正在檢查檔案衝突"

#: ../src/transaction.vala
msgid "Resolving dependencies"
msgstr "正在解決依賴關係"

#: ../src/transaction.vala
msgid "Checking inter-conflicts"
msgstr "正在檢查內部衝途"

#: ../src/transaction.vala
#, c-format
msgid "Installing %s"
msgstr "正在安裝 %s"

#: ../src/transaction.vala
#, c-format
msgid "Upgrading %s"
msgstr "正在升級 %s"

#: ../src/transaction.vala
#, c-format
msgid "Reinstalling %s"
msgstr "正在重新安裝 %s"

#: ../src/transaction.vala
#, c-format
msgid "Downgrading %s"
msgstr "正在降級 %s"

#: ../src/transaction.vala
#, c-format
msgid "Removing %s"
msgstr "正在移除 %s"

#: ../src/transaction.vala
msgid "Checking integrity"
msgstr "正在檢查軟體包完整性"

#: ../src/transaction.vala
msgid "Loading packages files"
msgstr "正在載入套件檔案"

#: ../src/transaction.vala
#, c-format
msgid "Configuring %s"
msgstr "正在設定 %s"

#: ../src/transaction.vala
msgid "Downloading"
msgstr "正在下載"

#: ../src/transaction.vala
#, c-format
msgid "Downloading %s"
msgstr "正在下載 %s"

#: ../src/transaction.vala
msgid "Checking available disk space"
msgstr "正在檢查可用磁碟空間"

#: ../src/transaction.vala
#, c-format
msgid "%s optionally requires %s"
msgstr "%s 選擇性需要 %s"

#: ../src/transaction.vala
#, c-format
msgid "Database file for %s does not exist"
msgstr "%s 資料庫檔案不存在"

#: ../src/transaction.vala
msgid "Checking keyring"
msgstr "正在檢查鑰匙圈"

#: ../src/transaction.vala
msgid "Downloading required keys"
msgstr "正在下載需要的鑰匙"

#: ../src/transaction.vala
#, c-format
msgid "%s installed as %s.pacnew"
msgstr "%s 已安裝為 %s.pacnew"

#: ../src/transaction.vala
#, c-format
msgid "%s installed as %s.pacsave"
msgstr "%s 已安裝為 %s.pacsave"

#: ../src/transaction.vala
msgid "Running pre-transaction hooks"
msgstr "正在執行前處理掛鉤"

#: ../src/transaction.vala
msgid "Running post-transaction hooks"
msgstr "正在執行後處理掛鉤"

#: ../src/transaction.vala
#, c-format
msgid "About %u seconds remaining"
msgstr "大約剩餘 %u 秒"

#: ../src/transaction.vala
#, c-format
msgid "About %lu minute remaining"
msgid_plural "About %lu minutes remaining"
msgstr[0] "大約剩餘 %lu 分鐘"

#: ../src/transaction.vala
#, c-format
msgid "Refreshing %s"
msgstr "正在更新 %s 內容"

#: ../src/transaction.vala
msgid "Error"
msgstr "錯誤"

#: ../src/transaction.vala
msgid "Warning"
msgstr "警告"

#: ../src/transaction.vala ../data/interface/progress_dialog.ui
#: ../data/interface/history_dialog.ui
msgid "_Close"
msgstr "關閉(_C)"

#: ../src/transaction.vala
msgid "Nothing to do"
msgstr "已無事可作"

#: ../src/transaction.vala
msgid "Transaction successful"
msgstr ""

#: ../src/transaction.vala
msgid "The transaction has been completed successfully"
msgstr ""

#: ../src/transaction.vala
msgid "Transaction successfully finished"
msgstr "事務處理成功完成"

#: ../src/transaction.vala
msgid "Transaction failed"
msgstr ""

#: ../src/transaction.vala
msgid "The transaction failed and no packages have been updated/installed"
msgstr ""

#: ../src/pamac-install/installer.vala
msgid "Unable to lock database!"
msgstr ""

#: ../src/pamac-tray/tray.vala ../src/pamac-manager/manager_window.vala
msgid "Your system is up-to-date"
msgstr "您的系統已經是最新的"

#: ../src/pamac-tray/tray.vala ../src/pamac-manager/manager_window.vala
msgid "Package Manager"
msgstr "套件管理員"

#: ../src/pamac-tray/tray.vala
msgid "_Quit"
msgstr "離開(_Q)"

#: ../src/pamac-tray/tray.vala
#, c-format
msgid "%u available update"
msgid_plural "%u available updates"
msgstr[0] "有 %u 個可用的更新"

#: ../src/pamac-tray/tray.vala ../src/pamac-manager/manager_window.vala
#: ../data/interface/progress_dialog.ui ../data/interface/manager_window.ui
msgid "Details"
msgstr "細節"

#: ../src/pamac-manager/manager.vala
msgid "Pamac is already running"
msgstr "Pamac仍在執行中"

#: ../src/pamac-manager/manager.vala
msgid "Refresh Databases"
msgstr ""

#: ../src/pamac-manager/manager.vala ../data/interface/manager_window.ui
msgid "View History"
msgstr "檢閱歷史"

#: ../src/pamac-manager/manager.vala ../src/pamac-manager/manager_window.vala
#: ../data/interface/manager_window.ui
msgid "Install Local Packages"
msgstr "安裝本機套件"

#: ../src/pamac-manager/manager.vala ../data/interface/manager_window.ui
msgid "Preferences"
msgstr "偏好設定"

#: ../src/pamac-manager/manager.vala ../data/interface/manager_window.ui
msgid "About"
msgstr "關於"

#: ../src/pamac-manager/manager.vala
msgid "Quit"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Deselect"
msgstr "取消選擇"

#: ../src/pamac-manager/manager_window.vala
msgid "Upgrade"
msgstr "升級"

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Install"
msgstr "安裝"

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Remove"
msgstr "移除"

#: ../src/pamac-manager/manager_window.vala
msgid "Waiting for another package manager to quit"
msgstr "正在等待其他套件管理員結束"

#: ../src/pamac-manager/manager_window.vala
#, c-format
msgid "%u pending operation"
msgid_plural "%u pending operations"
msgstr[0] "%u 個擱置中的動作"

#: ../src/pamac-manager/manager_window.vala
msgid "Installed"
msgstr "已安裝"

#: ../src/pamac-manager/manager_window.vala
msgid "Orphans"
msgstr "孤立的"

#: ../src/pamac-manager/manager_window.vala
msgid "Foreign"
msgstr "外來的"

#: ../src/pamac-manager/manager_window.vala
msgid "Pending"
msgstr "擱置中"

#: ../src/pamac-manager/manager_window.vala
msgid "Install Reason"
msgstr "安裝原因"

#: ../src/pamac-manager/manager_window.vala
msgid "Mark as explicitly installed"
msgstr "標示為單獨指定安裝"

#: ../src/pamac-manager/manager_window.vala
msgid "Licenses"
msgstr "授權條款"

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Repository"
msgstr "套件庫"

#: ../src/pamac-manager/manager_window.vala
msgid "Download size"
msgstr ""

#: ../src/pamac-manager/manager_window.vala
msgid "Installed size"
msgstr ""

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Groups"
msgstr "套件群組"

#: ../src/pamac-manager/manager_window.vala
msgid "Packager"
msgstr "打包者"

#: ../src/pamac-manager/manager_window.vala
msgid "Build Date"
msgstr "構建日期"

#: ../src/pamac-manager/manager_window.vala
msgid "Install Date"
msgstr "安裝日期"

#: ../src/pamac-manager/manager_window.vala
msgid "Signatures"
msgstr "數位簽章"

#: ../src/pamac-manager/manager_window.vala
msgid "Backup files"
msgstr "備份檔案"

#: ../src/pamac-manager/manager_window.vala
msgid "Depends On"
msgstr "依賴於"

#: ../src/pamac-manager/manager_window.vala
msgid "Optional Dependencies"
msgstr "選擇性依賴關係"

#: ../src/pamac-manager/manager_window.vala
msgid "Required By"
msgstr "被需要"

#: ../src/pamac-manager/manager_window.vala
msgid "Optional For"
msgstr "為後面這些軟體包的可選依賴"

#: ../src/pamac-manager/manager_window.vala
msgid "Provides"
msgstr "提供"

#: ../src/pamac-manager/manager_window.vala
msgid "Replaces"
msgstr "取代"

#: ../src/pamac-manager/manager_window.vala
msgid "Conflicts With"
msgstr "與下列套件衝突"

#: ../src/pamac-manager/manager_window.vala
msgid "Package Base"
msgstr "套件基礎"

#: ../src/pamac-manager/manager_window.vala
msgid "Maintainer"
msgstr "維護者"

#: ../src/pamac-manager/manager_window.vala
msgid "First Submitted"
msgstr "首次遞交"

#: ../src/pamac-manager/manager_window.vala
msgid "Last Modified"
msgstr "最後修改"

#: ../src/pamac-manager/manager_window.vala
msgid "Votes"
msgstr "投票"

#: ../src/pamac-manager/manager_window.vala
msgid "Out of Date"
msgstr "過期"

#: ../src/pamac-manager/manager_window.vala
msgid "Make Dependencies"
msgstr "構建時依賴關係"

#: ../src/pamac-manager/manager_window.vala
msgid "Check Dependencies"
msgstr "檢查用依賴關係"

#: ../src/pamac-manager/manager_window.vala ../data/interface/manager_window.ui
msgid "Name"
msgstr "名稱"

#: ../src/pamac-manager/manager_window.vala
#: ../data/interface/transaction_sum_dialog.ui
#: ../data/interface/manager_window.ui
#: ../data/interface/choose_ignorepkgs_dialog.ui
msgid "_Cancel"
msgstr "取消(_C)"

#: ../src/pamac-manager/manager_window.vala
msgid "_Open"
msgstr "開啟(_O)"

#: ../src/pamac-manager/manager_window.vala
msgid "Alpm Package"
msgstr "Alpm 軟體包"

#: ../src/pamac-manager/manager_window.vala
msgid "A Gtk3 frontend for libalpm"
msgstr "一個 libalpm 的 Gtk3 前端"

#: ../src/preferences_dialog.vala ../data/interface/preferences_dialog.ui
msgid "How often to check for updates, value in hours"
msgstr "請選擇您想要檢查更新的頻率，單位是小時"

#: ../src/preferences_dialog.vala ../data/interface/preferences_dialog.ui
msgid "Number of versions of each package to keep in the cache"
msgstr "每個軟體包要保留在快取中的版本數量"

#: ../src/preferences_dialog.vala
msgid "Build directory"
msgstr ""

#: ../src/preferences_dialog.vala
msgid "Package directory"
msgstr ""

#: ../src/preferences_dialog.vala
msgid "Worldwide"
msgstr "全世界"

#: ../src/preferences_dialog.vala
msgid "Speed"
msgstr "速度"

#: ../src/preferences_dialog.vala
msgid "Random"
msgstr "隨機"

#: ../data/polkit/org.pamac.policy.in
msgid "Authentication is required"
msgstr "需要授權"

#: ../data/interface/choose_provider_dialog.ui
msgid "Choose a Provider"
msgstr "選擇一個提供者"

#: ../data/interface/choose_provider_dialog.ui
#: ../data/interface/choose_ignorepkgs_dialog.ui
msgid "Choose"
msgstr "選擇"

#: ../data/interface/progress_dialog.ui
msgid "Progress"
msgstr "進度"

#: ../data/interface/history_dialog.ui
msgid "Pamac History"
msgstr "Pamac 歷史"

#: ../data/interface/transaction_sum_dialog.ui
msgid "Transaction Summary"
msgstr "事務處理概要"

#: ../data/interface/transaction_sum_dialog.ui
msgid "Commit"
msgstr "遞交"

#: ../data/interface/manager_window.ui
msgid "Refresh databases"
msgstr "刷新套件庫"

#: ../data/interface/manager_window.ui
msgid "Search"
msgstr "搜尋"

#: ../data/interface/manager_window.ui
msgid "State"
msgstr "狀態"

#: ../data/interface/manager_window.ui
msgid "Repositories"
msgstr "套件庫"

#: ../data/interface/manager_window.ui
msgid "Updates"
msgstr "更新"

#: ../data/interface/manager_window.ui
msgid "Version"
msgstr "版本"

#: ../data/interface/manager_window.ui
msgid "Size"
msgstr "大小"

#: ../data/interface/manager_window.ui
msgid "Popularity"
msgstr "人氣"

#: ../data/interface/manager_window.ui ../data/interface/preferences_dialog.ui
msgid "AUR"
msgstr "AUR"

#: ../data/interface/manager_window.ui
msgid "Browse"
msgstr "瀏覽"

#: ../data/interface/manager_window.ui
msgid "Reinstall"
msgstr "重新安裝"

#: ../data/interface/manager_window.ui
msgid "Dependencies"
msgstr "依賴關係"

#: ../data/interface/manager_window.ui
msgid "Files"
msgstr "檔案"

#: ../data/interface/manager_window.ui
msgid "_Apply"
msgstr "套用(_A)"

#: ../data/interface/preferences_dialog.ui
msgid "Remove unrequired dependencies"
msgstr "移除不需要的相依性"

#: ../data/interface/preferences_dialog.ui
msgid ""
"When removing a package, also remove its dependencies that are not required "
"by other packages"
msgstr "當移除一個軟體包時，同時移除它的不被其他軟體包需要的相依性"

#: ../data/interface/preferences_dialog.ui
msgid "Check available disk space"
msgstr "檢查可用磁碟空間"

#: ../data/interface/preferences_dialog.ui
msgid "Check for updates"
msgstr "檢查更新"

#: ../data/interface/preferences_dialog.ui
msgid "Update files databases (more details but slower)"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Hide tray icon when no update available"
msgstr "當沒有可用的更新時，隱藏系統匣圖示"

#: ../data/interface/preferences_dialog.ui
msgid "Ignore upgrades for:"
msgstr "忽略以下更新："

#: ../data/interface/preferences_dialog.ui
msgid "General"
msgstr "一般"

#: ../data/interface/preferences_dialog.ui
msgid "Background color:"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Text color:"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Font:"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Terminal"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Use mirrors from:"
msgstr "使用鏡像從："

#: ../data/interface/preferences_dialog.ui
msgid "Sort mirrors by:"
msgstr "排序鏡像伺服器由："

#: ../data/interface/preferences_dialog.ui
msgid "Refresh Mirrors List"
msgstr "重新整理鏡像列表"

#: ../data/interface/preferences_dialog.ui
msgid "Official Repositories"
msgstr "官方套件庫"

#: ../data/interface/preferences_dialog.ui
msgid ""
"AUR is a community maintained repository so it presents potential risks and "
"problems.\n"
"All AUR users should be familiar with the build process."
msgstr ""
"AUR 是一個社群維護的套件庫，所以它可能會有一些潛在的危險與問題。\n"
"所有 AUR 的使用者都應該要對構建過程有相當程度的了解。"

#: ../data/interface/preferences_dialog.ui
msgid "Enable AUR support"
msgstr "啟用 AUR 支援"

#: ../data/interface/preferences_dialog.ui
msgid "Allow Pamac to search and install packages from AUR"
msgstr "允許 Pamac 搜尋及安裝 AUR 上的軟體包"

#: ../data/interface/preferences_dialog.ui
msgid "Search in AUR by default"
msgstr "預設在 AUR 中搜尋"

#: ../data/interface/preferences_dialog.ui
msgid "Check for updates from AUR"
msgstr "自 AUR 檢查更新"

#: ../data/interface/preferences_dialog.ui
msgid "Keep built packages"
msgstr ""

#: ../data/interface/preferences_dialog.ui
msgid "Remove only the versions of uninstalled packages"
msgstr "僅移除未安裝的軟體包版本"

#: ../data/interface/preferences_dialog.ui
msgid "Clean cache"
msgstr "清除快取"

#: ../data/interface/preferences_dialog.ui
msgid "Cache"
msgstr "快取"

#: ../data/interface/choose_ignorepkgs_dialog.ui
msgid "Choose Ignored Upgrades"
msgstr "選取已忽略的更新"

#~ msgid "Checking delta integrity"
#~ msgstr "正在檢查增量包完整性"

#~ msgid "Applying deltas"
#~ msgstr "正在套用增量包"

#, c-format
#~ msgid "Generating %s with %s"
#~ msgstr "正在生成 %s (使用 %s)"

#~ msgid "Generation succeeded"
#~ msgstr "生成成功"

#~ msgid "Generation failed"
#~ msgstr "生成失敗"
